AWSTemplateFormatVersion: "2010-09-09"
Description: 
  S3 and CloudFront for Static website hosting (OAI Available)

Metadata:
  "AWS::CloudFormation::Interface":
    ParameterGroups:
      - Label: 
          default: "Root Stack Name"
        Parameters: 
          - RootStackName

Parameters:
  RootStackName:
    Description: Enter the root stack name for importing them outputs
    Type: String
    MinLength: 1
    MaxLength: 41
    AllowedPattern: ^[a-zA-Z0-9-_]*$

Resources:
  PCDNBucket:
    Type: "AWS::S3::Bucket"
    Properties:
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        IgnorePublicAcls: true
        BlockPublicPolicy: false
        RestrictPublicBuckets: true
      BucketName: !Join
        - "-"
        - - !Sub "geekworker.pcdn"
          - !Select
            - 0
            - !Split
              - "-"
              - !Select
                - 2
                - !Split
                  - "/"
                  - !Ref "AWS::StackId"
      Tags: 
        - Key: Name
          Value: !Sub "${RootStackName}-pcdn-bucket"
  
  CDNBucket:
    Type: "AWS::S3::Bucket"
    Properties:
      AccessControl: PublicRead
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        IgnorePublicAcls: true
        BlockPublicPolicy: false
        RestrictPublicBuckets: true
      BucketName: !Join
        - "-"
        - - !Sub "geekworker.cdn"
          - !Select
            - 0
            - !Split
              - "-"
              - !Select
                - 2
                - !Split
                  - "/"
                  - !Ref "AWS::StackId"
      Tags: 
        - Key: Name
          Value: !Sub "${RootStackName}-cdn-bucket"

  PCDNCloudFrontOriginAccessIdentity:
    Type: "AWS::CloudFront::CloudFrontOriginAccessIdentity"
    Properties:
      CloudFrontOriginAccessIdentityConfig:
        Comment: !Sub "access-identity-${PCDNBucket}"
  
  CDNCloudFrontOriginAccessIdentity:
    Type: "AWS::CloudFront::CloudFrontOriginAccessIdentity"
    Properties:
      CloudFrontOriginAccessIdentityConfig:
        Comment: !Sub "access-identity-${CDNBucket}"

  PCDNBucketPolicy:
    Type: "AWS::S3::BucketPolicy"
    Properties:
      Bucket: !Ref PCDNBucket
      PolicyDocument:
        Statement:
        - Action: "s3:GetObject"
          Effect: Allow
          Resource: !Sub "arn:aws:s3:::${PCDNBucket}/*"
          Principal:
            CanonicalUser: !GetAtt PCDNCloudFrontOriginAccessIdentity.S3CanonicalUserId
        - Action: "s3:GetObject"
          Effect: Allow
          Sid: MultiRestrictPolicy
          Resource: 
            - !Sub "arn:aws:s3:::${PCDNBucket}"
            - !Sub "arn:aws:s3:::${PCDNBucket}/*"
          Principal: '*'
          Condition:
            StringEquals:
              'aws:SourceVpce':
                -
                  Fn::ImportValue:
                    !Sub "${RootStackName}-vpce-s3"
  
  CDNBucketPolicy:
    Type: "AWS::S3::BucketPolicy"
    Properties:
      Bucket: !Ref CDNBucket
      PolicyDocument:
        Statement:
        - Action: "s3:GetObject"
          Effect: Allow
          Resource: !Sub "arn:aws:s3:::${CDNBucket}/*"
          Principal:
            CanonicalUser: !GetAtt CDNCloudFrontOriginAccessIdentity.S3CanonicalUserId
        - Action: "s3:GetObject"
          Effect: Allow
          Sid: MultiRestrictPolicy
          Resource: 
            - !Sub "arn:aws:s3:::${CDNBucket}"
            - !Sub "arn:aws:s3:::${CDNBucket}/*"
          Principal: '*'
          Condition:
            StringEquals:
              'aws:SourceVpce':
                -
                  Fn::ImportValue:
                    !Sub "${RootStackName}-vpce-s3"

Outputs:
  PCDNBucket:
    Value: !Ref PCDNBucket
    Export:
      Name: !Sub "${RootStackName}-pcdn-bucket"
  
  PCDNBucketName:
    Value: !GetAtt PCDNBucket.DomainName
    Export:
      Name: !Sub "${RootStackName}-pcdn-bucket-name"

  CDNBucket:
    Value: !Ref CDNBucket
    Export:
      Name: !Sub "${RootStackName}-cdn-bucket"
  
  CDNBucketName:
    Value: !GetAtt CDNBucket.DomainName
    Export:
      Name: !Sub "${RootStackName}-cdn-bucket-name"

  PCDNCloudFrontOriginAccessIdentity:
    Value: !Ref PCDNCloudFrontOriginAccessIdentity
    Export:
      Name: !Sub "${RootStackName}-pcdn-cloudfront-origin-access-identity"
  
  CDNCloudFrontOriginAccessIdentity:
    Value: !Ref CDNCloudFrontOriginAccessIdentity
    Export:
      Name: !Sub "${RootStackName}-cdn-cloudfront-origin-access-identity"